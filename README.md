#  jQuery-TypeWriter #
A jQuery plugin for creating typewriter text effect on HTML tags.

Dependency
jQuery 1.11
[Download audio](http://vocaroo.com/i/s0S6ygPa8T0I)
Usage

```
#!javascript

$(selector).typeWriter({txt:message, audioSrc:source, interval: milliseconds});
```


Example

```
#!javascript

$("h1").typeWriter({txt:"Happy Bday", audioSrc:"example.ogg", interval:200});
```